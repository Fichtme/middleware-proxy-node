# Middleware proxy node
Simple function (for gcp) that proxies requests using express and `http-proxy-middleware`.

## Installation
```shell
npm install
cp config.example.js config.js #update the proxy url
```

## development

```shell
npm start
```

## production gcp
just zip the package and upload to gcp cloud function or create your own pipeline.