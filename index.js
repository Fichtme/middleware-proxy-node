const express = require('express');
const config = require('./config');
const proxy = require('http-proxy-middleware');
const bodyParser = require('body-parser');
const queryString = require('querystring');

const proxyInstance = getProxy();

const app = express();
app.use('', (req, res, next) => {
	if (req.method === 'OPTIONS') {
		res.setHeader('Access-Control-Allow-Origin', '*');
		res.setHeader('Access-Control-Allow-Methods', 'GET, POST, OPTIONS, PUT, PATCH, DELETE');
		res.setHeader('Access-Control-Allow-Headers', '*');
		res.setHeader('Access-Control-Allow-Credentials', true);
		res.sendStatus(200);
	} else {
		next();
	}
});

app.use('/', proxyInstance);
// app.listen(3000); #uncomment this part to allow local development on port

app.use(bodyParser.json());
app.use(bodyParser.urlencoded({extended: true}));

function getProxy() {
	return proxy(getConfig());
}

function getConfig() {
	return {
		target: config.destination,
		changeOrigin: true,
		followRedirects: true,
		secure: true,
		/**
		 * @param {import('http').ClientRequest} proxyReq
		 * @param {import('http').IncomingMessage} req
		 * @param {import('http').ServerResponse} res
		 */
		onProxyReq: (proxyReq, req, res) => {
			/**
			 * @type {null | undefined | object}
			 */
			const body = req.body;

			res.setHeader('Access-Control-Allow-Origin', '*');
			res.setHeader('Access-Control-Allow-Methods', 'GET, POST, OPTIONS, PUT, PATCH, DELETE');
			res.setHeader('Access-Control-Allow-Headers', '*');
			res.setHeader('Access-Control-Allow-Credentials', true);

			if (!body || !Object.keys(body).length) {
				return;
			}
			const contentType = proxyReq.getHeader('Content-Type');
			let contentTypeStr = Array.isArray(contentType) ? contentType[0] : contentType.toString();
			contentTypeStr = contentTypeStr.match(/^([^;]*)/)[1];

			let bodyData;
			if (contentTypeStr === 'application/json') {
				bodyData = JSON.stringify(body);
			}

			if (contentTypeStr === 'application/x-www-form-urlencoded') {
				bodyData = queryString.stringify(body);
			}

			if (bodyData) {
				proxyReq.setHeader('Content-Length', Buffer.byteLength(bodyData));
				proxyReq.write(bodyData);
			}
		}
	};
}

module.exports = {
	proxy: app
}